package net.gadgetlab_yokosuka.android.androidsampleapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// イベントハンドラの設定
		Button op_ok = (Button) findViewById(R.id.op_ok);
		op_ok.setOnClickListener(this);
	}

/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
*/

	@Override
	public void onClick(View arg0) {
		// ビューの取得
		EditText in_edit = (EditText) findViewById(R.id.in_edit);
		TextView out_edit = (TextView) findViewById(R.id.out_edit);
		// 文字列の取得
		String text = in_edit.getText().toString();
		// 文字列の出力
		out_edit.setText(text);
	}

}
